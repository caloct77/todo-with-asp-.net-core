using System;

namespace rad4mtest
{
    public class ToDo
    {

        public string Td_Title { get; set; }

        public string Td_Expiration { get; set; }

        public string Td_Desc { get; set; }

        public int Td_Completion { get; set; }

    }
}
