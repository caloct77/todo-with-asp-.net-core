﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Logging;

namespace rad4mtest.Controllers
{


    [ApiController]
    [Route("api/[controller]")]
    public class ToDoController : ControllerBase
    {
        
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<ToDoController> _logger;

        public ToDoController(ILogger<ToDoController> logger)
        {
            _logger = logger;
        }

        //GET : api/ToDo -> GET ALL
        [HttpGet]
        public  IEnumerable<ToDo> Get()
        {
          ToDo[] allRecords = null;
          var list = new List<ToDo>();
          //CONNECTION
          SqliteConnectionStringBuilder connectionStringBuilder = new SqliteConnectionStringBuilder();
          connectionStringBuilder.DataSource = "db_ToDo.db";
          SqliteConnection connection = new SqliteConnection(connectionStringBuilder.ConnectionString);
          connection.Open();
          //QUERY
          var sqlSelect = connection.CreateCommand();
          sqlSelect.CommandText = "SELECT * FROM tbl_todo";
          var reader = sqlSelect.ExecuteReader();

          //MAKE ARRAY
          while (reader.Read()){
                list.Add(new ToDo { Td_Title = reader.GetString(1), Td_Expiration = reader.GetString(2),
                    Td_Desc = reader.GetString(3), Td_Completion = reader.GetInt32(4)});

            }
          allRecords = list.ToArray();
            return allRecords;
        }

        //GET : api/ToDo/2 -> GET BY ID
        [HttpGet("{id}")]
        public IEnumerable<ToDo> Get(int id)
        {
            ToDo[] allRecords = null;
            var list = new List<ToDo>();
            //CONNECTION
            SqliteConnectionStringBuilder connectionStringBuilder = new SqliteConnectionStringBuilder();
            connectionStringBuilder.DataSource = "db_ToDo.db";
            SqliteConnection connection = new SqliteConnection(connectionStringBuilder.ConnectionString);
            connection.Open();
            //QUERY
            var sqlSelect = connection.CreateCommand();
            sqlSelect.CommandText = "SELECT * FROM tbl_todo WHERE Id = "+ id.ToString();
            var reader = sqlSelect.ExecuteReader();

            //MAKE ARRAY
            while (reader.Read())
            {
                list.Add(new ToDo
                {
                    Td_Title = reader.GetString(1),
                    Td_Expiration = reader.GetString(2),
                    Td_Desc = reader.GetString(3),
                    Td_Completion = reader.GetInt32(4)
                });

            }
            allRecords = list.ToArray();
            return allRecords;
        }
        // POST : api/Movies -> INSERT
        [HttpPost]
        public void Post(ToDo todo)
        {
            
            //CONNECTION
            SqliteConnectionStringBuilder connectionStringBuilder = new SqliteConnectionStringBuilder();
            connectionStringBuilder.DataSource = "db_ToDo.db";
            SqliteConnection connection = new SqliteConnection(connectionStringBuilder.ConnectionString);
            connection.Open();
            //QUERY
            var sqlInsert = connection.CreateCommand();
            sqlInsert.CommandText = "INSERT INTO tbl_todo VALUES('"+todo.Td_Title+"','"+todo.Td_Expiration+"','"+todo.Td_Desc+"',"+todo.Td_Completion+")";
            sqlInsert.ExecuteNonQuery();

        }
        // PUT : api/todo/2 -> UPDATE
        [HttpPut("{id}")]
        public void Put(int id, ToDo todo)
        {

            //CONNECTION
            SqliteConnectionStringBuilder connectionStringBuilder = new SqliteConnectionStringBuilder();
            connectionStringBuilder.DataSource = "db_ToDo.db";
            SqliteConnection connection = new SqliteConnection(connectionStringBuilder.ConnectionString);
            connection.Open();
            //QUERY
            var sqlInsert = connection.CreateCommand();
            sqlInsert.CommandText = "UPDATE tbl_todo SET td_Title = '" + todo.Td_Title + "', td_Expiration = '" + todo.Td_Expiration + "', td_Desc = '" + todo.Td_Desc + "', td_Completion = " + todo.Td_Completion + " WHERE Id = " + id + ";";
       
            sqlInsert.ExecuteNonQuery();

        }
        //PUT : api/todo/2/82 -> UPDATE COMPLETION
        [HttpPut("{id}/{percentage}")]

        public void Put(int id, int percentage)
        {

            //CONNECTION
            SqliteConnectionStringBuilder connectionStringBuilder = new SqliteConnectionStringBuilder();
            connectionStringBuilder.DataSource = "db_ToDo.db";
            SqliteConnection connection = new SqliteConnection(connectionStringBuilder.ConnectionString);
            connection.Open();
            //QUERY
            var sqlInsert = connection.CreateCommand();
            sqlInsert.CommandText = "UPDATE tbl_todo SET td_Completion = " + percentage + " WHERE Id = " + id + ";";

            sqlInsert.ExecuteNonQuery();

        }

        //PUT : api/todo/2/82 -> MARK AS DONE
        [HttpPut("{id}")]

        public void Put(int id)
        {

            //CONNECTION
            SqliteConnectionStringBuilder connectionStringBuilder = new SqliteConnectionStringBuilder();
            connectionStringBuilder.DataSource = "db_ToDo.db";
            SqliteConnection connection = new SqliteConnection(connectionStringBuilder.ConnectionString);
            connection.Open();
            //QUERY
            var sqlInsert = connection.CreateCommand();
            sqlInsert.CommandText = "UPDATE tbl_todo SET td_Completion = 100 WHERE Id = " + id + ";";

            sqlInsert.ExecuteNonQuery();

        }

        //DELETE : api/todo/1 -> DELETE
        [HttpDelete]
        [Route("api/{id}")]
        public void Delete(int id)
        {

            //CONNECTION
            SqliteConnectionStringBuilder connectionStringBuilder = new SqliteConnectionStringBuilder();
            connectionStringBuilder.DataSource = "db_ToDo.db";
            SqliteConnection connection = new SqliteConnection(connectionStringBuilder.ConnectionString);
            connection.Open();
            //QUERY
            var sqlInsert = connection.CreateCommand();
            sqlInsert.CommandText = "DELETE FROM tbl_todo WHERE Id = " + id + ";";

            sqlInsert.ExecuteNonQuery();

        }
    }
}
